/* Includes ------------------------------------------------------------------*/
#include "main.h"

int main(void)
{
	/*Start - Order Functions*/
	vRCC_Init();
	vITM_Init();
	vDWT_Init();
	/*End - Order Functions*/

	vGPIO_Init();

	for(;;)
	{
		printf_("Version: %f\r\n", (float)1.2);
		GPIO_ToggleBits(LED_NUCLEO144_PORT, LED_NUCLEO144_LED1_PIN);
		GPIO_ToggleBits(LED_NUCLEO144_PORT, LED_NUCLEO144_LED2_PIN);
		GPIO_ToggleBits(LED_NUCLEO144_PORT, LED_NUCLEO144_LED3_PIN);
		vDWT_Delay(500, eDWT_ms);
	}
}

void vGPIO_Init(void)
{
	GPIO_InitTypeDef xGPIO;
	GPIO_StructInit(&xGPIO);

	xGPIO.GPIO_Mode = GPIO_Mode_OUT;
	xGPIO.GPIO_Speed = GPIO_Fast_Speed;
	xGPIO.GPIO_OType = GPIO_OType_PP;
	xGPIO.GPIO_PuPd = GPIO_PuPd_DOWN;
	xGPIO.GPIO_Pin = LED_NUCLEO144_LED1_PIN | LED_NUCLEO144_LED2_PIN | LED_NUCLEO144_LED3_PIN;

	GPIO_Init(LED_NUCLEO144_PORT, &xGPIO);

	GPIO_ResetBits(LED_NUCLEO144_PORT, LED_NUCLEO144_LED1_PIN);
	GPIO_ResetBits(LED_NUCLEO144_PORT, LED_NUCLEO144_LED2_PIN);
	GPIO_ResetBits(LED_NUCLEO144_PORT, LED_NUCLEO144_LED3_PIN);
}

void vRCC_Init(void)
{
	/*************************/

	SysTick_Config( SystemCoreClock / 1000 );
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0U, 0U));

	/*************************/

	RCC_DeInit();
	RCC_HSICmd(ENABLE);
	while(RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET){};
	RCC_AdjustHSICalibrationValue(0x10);

	RCC_PLLConfig(RCC_PLLSource_HSI, 8, 180, 2, 2); //180MHz
	RCC_PLLCmd(ENABLE);
	while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET){};
	FLASH_SetLatency(FLASH_Latency_5);

	RCC_HCLKConfig(RCC_SYSCLK_Div1);
	RCC_PCLK1Config(RCC_HCLK_Div4);
	RCC_PCLK2Config(RCC_HCLK_Div2);
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

	SystemCoreClockUpdate();

	/**************************/

	RCC_ClocksTypeDef RCC_BaseStruct;
	RCC_GetClocksFreq(&RCC_BaseStruct);


	/**************************/

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);

	/*****************************/
}

void vITM_Init( void )
{
	uint32_t u32SWO_Speed = 6000000;
	uint32_t u32SWO_Prescaler = (SystemCoreClock / u32SWO_Speed) - 1;  // SWOSpeed in Hz

	CoreDebug->DEMCR = CoreDebug_DEMCR_TRCENA_Msk; /* enable trace in core debug */
	*((volatile unsigned *)(ITM_BASE + 0x400F0)) = 0x00000002; /* "Selected PIN Protocol Register": Select which protocol to use for trace output (2: SWO NRZ, 1: SWO Manchester encoding) */
	*((volatile unsigned *)(ITM_BASE + 0x40010)) = u32SWO_Prescaler; /* "Async Clock Prescaler Register". Scale the baud rate of the asynchronous output */
	*((volatile unsigned *)(ITM_BASE + 0x00FB0)) = 0xC5ACCE55; /* ITM Lock Access Register, C5ACCE55 enables more write access to Control Register 0xE00 :: 0xFFC */
	ITM->TCR = ITM_TCR_TraceBusID_Msk | ITM_TCR_SWOENA_Msk | ITM_TCR_SYNCENA_Msk | ITM_TCR_ITMENA_Msk; /* ITM Trace Control Register */
	ITM->TPR = ITM_TPR_PRIVMASK_Msk; /* ITM Trace Privilege Register */
	ITM->TER = 0x1; /* ITM Trace Enable Register. Enabled tracing on stimulus ports. One bit per stimulus port. */
	*((volatile unsigned *)(ITM_BASE + 0x01000)) = 0x400003FE; /* DWT_CTRL */
	*((volatile unsigned *)(ITM_BASE + 0x40304)) = 0x00000100; /* Formatter and Flush Control Register */
}

void _putchar(char character)
{
	ITM_SendChar(character);
}

void vDWT_Init( void )
{
	/* Enable TRC Register */
	CoreDebug->DEMCR &= ~0x01000000; //Its necessary ???
	CoreDebug->DEMCR |=  0x01000000; //Its necessary ???

	/* Enable DWT counter */
	DWT->CTRL &= ~0x00000001;
	DWT->CTRL |=  0x00000001;

	/* Reset DWT counter */
	DWT->CYCCNT = 0;

	/* 3 NO OPERATION instructions */
	__ASM volatile ("NOP");
	__ASM volatile ("NOP");
	__ASM volatile ("NOP");

	/* Check if clock cycle counter has started */
	if(!DWT->CYCCNT)
	{
		while(1){};
	}
}

uint32_t u32DWT_GetTick( void )
{
	/* Get DWT counter */
	return (uint32_t)DWT->CYCCNT;

}

void vDWT_Delay( uint32_t u32Time, eDWT_TimeUnit eUnit )
{
	uint32_t u32Constant;

	switch(eUnit)
	{
	case eDWT_ms:
		u32Constant = 1000;
		break;

	case eDWT_us:
		u32Constant = 1000000;
		break;

	default :
		return;
	}

	/* Get the ticks for 1us */
	uint32_t u32Tick = SystemCoreClock / u32Constant;

	/* Get the total ticks for us delay value */
	volatile uint32_t v_u32TotalTick = (u32Time*u32Tick);

	/* Get the start tick */
	volatile uint32_t v_u32StartTick = u32DWT_GetTick();

	/* Wait for delay */
	while ((u32DWT_GetTick() - v_u32StartTick) < v_u32TotalTick){};
}

float fDWT_ConvTick( uint32_t u32Tick, eDWT_TimeUnit eUnit )
{
	uint32_t u32Constant;

	switch(eUnit)
	{
	case eDWT_ms:
		u32Constant = 1000;
		break;

	case eDWT_us:
		u32Constant = 1000000;
		break;

	default :
		return 0;
	}

	return (float) (u32Tick / (SystemCoreClock / u32Constant));
}
