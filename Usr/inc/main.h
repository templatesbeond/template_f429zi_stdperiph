/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include "printf.h"

/* Exported types ------------------------------------------------------------*/
typedef enum
{
	eDWT_ms = (uint8_t)0,
	eDWT_us
} eDWT_TimeUnit;

/* Exported constants --------------------------------------------------------*/
#define LED_NUCLEO144_LED1_PIN 		GPIO_Pin_0
#define LED_NUCLEO144_LED2_PIN 		GPIO_Pin_7
#define LED_NUCLEO144_LED3_PIN 		GPIO_Pin_14
#define LED_NUCLEO144_PORT 			GPIOB

/* Exported macro ------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */
void vRCC_Init( void );
void vGPIO_Init( void );

void vDWT_Init( void );
uint32_t u32DWT_GetTick( void );
void vDWT_Delay( uint32_t u32Time, eDWT_TimeUnit eUnit );
float fDWT_ConvTick( uint32_t u32Tick, eDWT_TimeUnit eUnit );

void vITM_Init( void );

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
